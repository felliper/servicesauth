<?php
namespace IbookingBR\ServicesAuth;


class AuthConfig{

    const URL_AUTH_SERVICE = 'https://servicesauth.ibookingpms.com.br';

    const ENDPOINT_VALIDATE_TOKEN = '/api/validatetoken';

}